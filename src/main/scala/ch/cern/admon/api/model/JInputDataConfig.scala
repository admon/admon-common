package ch.cern.admon.api.model

import scala.collection.JavaConverters._
import ch.cern.admon.core.model.{InputDataConfig, JoinConfig, SourceConfig, SourceFields, TransformConfig}

case class JInputDataConfig(jSourceConfigs: java.util.List[JSourceConfig],
                            jJoinConfig: JJoinConfig = null,
                            jFinalTransformConfig: JTransformConfig = null,
                            jGroupByFields: java.util.List[java.lang.String] = null) {
  def asScala: InputDataConfig = {
    InputDataConfig(
      sourceConfigs = jSourceConfigs.asScala.map((jsc: JSourceConfig) => jsc.asScala).toList,
      joinConfig = if (jJoinConfig != null) Some(jJoinConfig.asScala) else None,
      finalTransformConfig = if (jFinalTransformConfig != null) Some(jFinalTransformConfig.asScala) else None,
      groupByFields = if (jGroupByFields != null) Some(jGroupByFields.asScala.toList) else None
    )
  }
}

case class JSourceConfig(jSourceFields: JSourceFields,
                         jTransformConfig: JTransformConfig) {
  def asScala: SourceConfig = {
    SourceConfig(
      sourceFields = jSourceFields.asScala,
      transformConfig = jTransformConfig.asScala
    )
  }
}

case class JSourceFields(producer: java.lang.String,
                         typePrefix: java.lang.String,
                         topicType: java.lang.String) {
  def asScala: SourceFields = {
    SourceFields(producer = producer, typePrefix = typePrefix, topicType = topicType)
  }
}

case class JTransformConfig(jFilterExpression: java.lang.String = null,
                            jSelectFields: java.util.ArrayList[java.lang.String] = null,
                            jRenameMap: java.util.HashMap[java.lang.String, java.lang.String] = null) {
  def asScala: TransformConfig = {
    TransformConfig(
      filterExpression = Some(jFilterExpression),
      selectFields = if(jSelectFields != null) Some(jSelectFields.asScala.toList) else None,
      renameMap = if(jRenameMap != null) Some(jRenameMap.asScala.toMap) else None
    )
  }
}

case class JJoinConfig(jAggIntervalSeconds: java.lang.Long,
                       jAggMethod: java.lang.String) {
  def asScala: JoinConfig = {
    JoinConfig(jAggIntervalSeconds, jAggMethod)
  }
}
