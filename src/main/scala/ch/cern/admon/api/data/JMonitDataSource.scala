package ch.cern.admon.api.data

import scala.collection.JavaConverters._
import ch.cern.admon.api.model.{JSourceConfig, JInputDataConfig}
import ch.cern.admon.core.data.MonitDataSource
import org.apache.spark.sql.{DataFrame, SparkSession}


class JMonitDataSource(spark: SparkSession,
                       jInputDataConfig: JInputDataConfig = null) {

  def this(spark: SparkSession, jSourceConfig: JSourceConfig) = this(
    spark,
    JInputDataConfig(List(jSourceConfig).asJava)
  )

  var monitDataSource: MonitDataSource = new MonitDataSource(spark, jInputDataConfig.asScala)

  def readKafka(startTime: java.lang.Long,
                endTime: java.lang.Long,
                kafkaInputBrokers: java.lang.String,
                kafkaPassword: java.lang.String): DataFrame = {
    monitDataSource.readKafka(startTime, endTime, kafkaInputBrokers, kafkaPassword)
  }

  def readHDFS(startTime: java.lang.Long,
               endTime: java.lang.Long): DataFrame = {
    monitDataSource.readHDFS(startTime, endTime)
  }
}
