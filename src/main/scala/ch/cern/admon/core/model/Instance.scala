package ch.cern.admon.core.model

import scala.collection.mutable

case class Instance(
  metadata: Map[String, String],
  data: List[Map[String, String]])

case class AggregateContent(
  metadata: mutable.Map[String, String],
  data: mutable.ListBuffer[Map[String, String]])