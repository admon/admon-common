package ch.cern.admon.core.model


case class DetectionEntity(id: Integer,
                           projectId: Integer,
                           title: String,
                           intervalMinutes: Long,
                           inputDataConfig: InputDataConfig,
                           inferenceConfig: InferenceConfig,
                           monitInformation: MonitInformation)

/**
 * Features configs describes configurations from kafka source to payload
 *
 * @param sourceConfigs kafka source topics, their filtering, selecting and renaming of columns.
 * @param joinConfig How to combine multiple source data frames
 * @param finalTransformConfig after joines, a final filter, select and rename happens
 * @param groupByFields fields to group by result
 */
case class InputDataConfig(sourceConfigs: List[SourceConfig],
                           joinConfig: Option[JoinConfig] = None,
                           finalTransformConfig: Option[TransformConfig] = None,
                           groupByFields: Option[List[String]] = None)

case class SourceConfig(sourceFields: SourceFields,
                        transformConfig: TransformConfig)

case class SourceFields(producer: String,
                        typePrefix: String,
                        topicType: String)

case class TransformConfig(filterExpression: Option[String] = None,
                           selectFields: Option[List[String]] = None,
                           renameMap: Option[Map[String, String]] = None)

case class JoinConfig(aggIntervalSeconds: Long,
                      aggMethod: String)

case class InferenceConfig(modelName: String, kubeflowNamespace: String)

case class MonitInformation(producer: String, labels: Map[String, String])
