package ch.cern.admon.core.model

case class DetectionTask(
                          id: Integer,
                          startDetection: Long,
                          endDetection: Long,
                          created: Long,
                          updated: Long,
                          detectionEntity: DetectionEntity,
                          state: String
                        ) {
  def updateState(newState: String): DetectionTask = {
    DetectionTask(
      id,
      startDetection,
      endDetection,
      created,
      updated,
      detectionEntity,
      newState
    )
  }
}
