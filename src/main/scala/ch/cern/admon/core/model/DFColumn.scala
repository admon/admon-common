package ch.cern.admon.core.model

object DFColumn extends Enumeration {
  type DFColumn = Value

  val SentToMonit: DFColumn.Value = Value("sent_to_monit")
  val Predictions: DFColumn.Value = Value("predictions")
  val Instance: DFColumn.Value = Value("instance")
  val Instances: DFColumn.Value = Value("instances")

  val Time: DFColumn.Value = Value("timestamp")
  val MetadataTime: DFColumn.Value = Value("metadata.timestamp")
  val JSONdata: DFColumn.Value = Value("value")

  val ContentLength: DFColumn.Value = Value("content_length")
  val CombineGroup: DFColumn.Value = Value("combine_group")
  val Partition: DFColumn.Value = Value("partition")
}
