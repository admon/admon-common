package ch.cern.admon.core.model

object TaskState extends Enumeration {
  type TaskState = Value

  val Progress: TaskState.Value = Value("progress")
  val Completed: TaskState.Value = Value("completed")
  val FailedInternal: TaskState.Value = Value("failed_internal")
  val FailedMonit: TaskState.Value = Value("failed_monit")
  val FailedInference: TaskState.Value = Value("failed_inference")
}

