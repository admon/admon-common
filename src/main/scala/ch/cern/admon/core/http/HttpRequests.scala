package ch.cern.admon.core.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import org.json4s.DefaultFormats
import ch.cern.admon.core.utils.AsyncAwait.await
import ch.cern.admon.core.utils.Constants.AUTH_SERVICE_SESSION

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object HttpRequests {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats

  private def buildAuthHeader = headers.RawHeader("Cookie", s"authservice_session=$AUTH_SERVICE_SESSION")

  def unpackResponse(responseFuture: Future[HttpResponse]): Future[String] = {
    Future {
      val response: HttpResponse = await(responseFuture)
      val postFuture = response.entity.toStrict(timeout = 1000.seconds).map { _.data.utf8String }
      val postString = await(postFuture)
      postString
    }
  }

  def singleRequest(requestType: HttpMethod, url: String, requestBody: Option[String] = None): String = {
    val httpEntity = HttpEntity(ContentTypes.`application/json`,requestBody.getOrElse(""))
    val request = HttpRequest(requestType, url, entity = httpEntity).withHeaders(buildAuthHeader)
    val result = Http().singleRequest(request)
    await(unpackResponse(result))
  }
}
