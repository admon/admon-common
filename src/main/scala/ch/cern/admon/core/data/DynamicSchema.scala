package ch.cern.admon.core.data

import ch.cern.admon.core.model.DFColumn
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.StructType


object DynamicSchema {
  implicit class DynamicSchemaImplicit(dataFrame: DataFrame) {
    def withDynamicSchema(sampleSize: Int = 100): DataFrame = {
      val sampleRows: Seq[String] = dataFrame.sample(0.1).take(sampleSize).map(row => row(0).toString)
      val schema = buildSchema(dataFrame.sparkSession, sampleRows)

      dataFrame.select(
        from_json(col(DFColumn.JSONdata.toString).cast("string"), schema)
          .alias("parsed")
      ).select("parsed.*")
    }

    def buildSchema(spark: SparkSession, jsonStrings: Seq[String]): StructType = {
      import spark.implicits._
      spark.read.json(jsonStrings.toDS).schema
    }
  }
}