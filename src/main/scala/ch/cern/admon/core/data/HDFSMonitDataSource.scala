package ch.cern.admon.core.data

import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import ch.cern.admon.core.model.SourceConfig
import ch.cern.admon.core.processing.InputConfigProcess
import ch.cern.admon.core.utils.TimestampFilter

class HDFSMonitDataSource(spark: SparkSession) {
  val LOGGER: Logger = Logger.getLogger(this.getClass.toString)

  def getDF(sourceConfig: SourceConfig, startTime: BigInt, endTime: BigInt): DataFrame = {
    val hdfsLocationService = HDFSMonitDataLocation(sourceConfig = sourceConfig)
    val (dataType, locations) = hdfsLocationService.buildPathWithDatePartitionFolders(startTime, endTime)
    var df = spark.read.format(dataType).load(locations:_*)
    df = TimestampFilter.dataFrameInInterval(df, sourceConfig.sourceFields.producer, startTime, endTime)
    InputConfigProcess.transformDF(df, sourceConfig.transformConfig)
  }
}
