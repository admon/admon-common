package ch.cern.admon.core.data

import java.time._
import java.time.format._
import java.time.temporal._
import org.apache.hadoop.fs.Path

import scala.collection.mutable.ListBuffer
import ch.cern.admon.core.utils.Constants.{HDFS_MONIT_ARCHIVE_ROOT_PATH, HDFS_MONIT_COLLECTD_ROOT_PATH}
import ch.cern.admon.core.model.SourceConfig

class HDFSMonitDataLocation(producer: String, typePrefix: String, monitType: String) {

  def buildDataLocationRootPath: (String, Path) = {
    if (producer == "collectd")
      buildCollectdDataRootPath
    else
      buildGenericDataRootPath
  }

  private def buildCollectdDataRootPath: (String, Path) = {
    ("parquet", new Path(HDFS_MONIT_COLLECTD_ROOT_PATH, monitType))
  }

  private def buildGenericDataRootPath: (String, Path) = {
    val rootPathWithProducer = new Path(HDFS_MONIT_ARCHIVE_ROOT_PATH, producer)
    if (monitType.isEmpty)
      ("json", new Path(rootPathWithProducer, typePrefix))
    else
      ("json", new Path(new Path(rootPathWithProducer, typePrefix), monitType))
  }

  def buildPathWithDatePartitionFolders(startTime: BigInt, endTime: BigInt): (String, List[String]) = {
    try {
      val start = LocalDateTime.ofEpochSecond(startTime.toLong, 0, ZoneOffset.UTC)
      val end = LocalDateTime.ofEpochSecond(endTime.toLong, 0, ZoneOffset.UTC)
      val timeDiffInDays = ChronoUnit.DAYS.between(start.toLocalDate, end.toLocalDate)

      val listOfResultHDFSPaths = new ListBuffer[String]()
      for (i <- 0 to timeDiffInDays.toInt) {
        listOfResultHDFSPaths += buildDataLocationRootPath._2
          .suffix("/".concat(DateTimeFormatter.ofPattern("yyyy/MM/dd")
            .format(start.plus(i, ChronoUnit.DAYS).atZone(ZoneOffset.UTC)))).toString
      }
      buildDataLocationRootPath._1 -> listOfResultHDFSPaths.toList
    } catch {
      case e: DateTimeParseException => throw new RuntimeException("Provided start/end time values should be in ISO " +
        "format e.g.`2011-12-03T10:15:30`")
    }
  }
}

object HDFSMonitDataLocation {
  def apply(sourceConfig: SourceConfig): HDFSMonitDataLocation = {
    val producer: String = sourceConfig.sourceFields.producer
    val typePrefix: String = sourceConfig.sourceFields.typePrefix
    val monitType: String = sourceConfig.sourceFields.topicType
    new HDFSMonitDataLocation(producer, typePrefix, monitType)
  }
}
