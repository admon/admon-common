package ch.cern.admon.core.data

import ch.cern.admon.core.model.{InputDataConfig, SourceConfig}
import ch.cern.admon.core.processing.InputConfigProcess
import org.apache.spark.sql.{DataFrame, SparkSession}

class MonitDataSource(spark: SparkSession, inputDataConfig: InputDataConfig) {

  def this(spark: SparkSession, sourceConfig: SourceConfig) = this(
    spark,
    InputDataConfig(sourceConfigs = List(sourceConfig))
  )

  def readKafka(startTime: Long, endTime: Long, kafkaInputBrokers: String, kafkaPassword: String): DataFrame = {
    val dfs = inputDataConfig.sourceConfigs.map((sc: SourceConfig) => {
      new KafkaMonitDataSource(spark, kafkaInputBrokers, kafkaPassword).getDF(
        sourceConfig = sc,
        startTime = BigInt(startTime),
        endTime = BigInt(endTime)
      )
    })
    InputConfigProcess.processInputConfig(dfs, inputDataConfig, startTime)
  }

  def readHDFS(startTime: Long, endTime: Long): DataFrame = {
    val dfs = inputDataConfig.sourceConfigs.map((sc: SourceConfig) => {
      new HDFSMonitDataSource(spark).getDF(
        sourceConfig = sc,
        startTime = BigInt(startTime),
        endTime = BigInt(endTime)
      )
    })
    InputConfigProcess.processInputConfig(dfs, inputDataConfig, startTime)
  }
}

