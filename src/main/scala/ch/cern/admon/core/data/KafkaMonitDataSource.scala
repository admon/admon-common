package ch.cern.admon.core.data

import DynamicSchema.DynamicSchemaImplicit
import ch.cern.admon.core.model.{DFColumn, InputDataConfig, SourceConfig}
import ch.cern.admon.core.processing.InputConfigProcess
import ch.cern.admon.core.utils.{DotEnv, TimestampFilter}
import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s.jackson.Serialization
import org.json4s.DefaultFormats


class KafkaMonitDataSource(spark: SparkSession,
                           trustStoreLocation: String,
                           keyStoreLocation: String,
                           kafkaInputBrokers: String,
                           kafkaPassword: String) {

  def this(spark: SparkSession, kafkaInputBrokers: String, kafkaPassword: String) = this(
    spark = spark,
    trustStoreLocation = "resources/truststore.jks",
    keyStoreLocation = "resources/keystore.jks",
    kafkaInputBrokers = kafkaInputBrokers,
    kafkaPassword = kafkaPassword
  )

  def this(spark: SparkSession) = this(
    spark = spark,
    kafkaPassword = DotEnv.get("KAFKA_PASSWORD"),
    kafkaInputBrokers = DotEnv.get("KAFKA_INPUT_BROKERS")
  )

  val LOGGER: Logger = Logger.getLogger(this.getClass.toString)
  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats

  val kafkaConf = Map(
    "kafka.ssl.truststore.location" -> trustStoreLocation,
    "kafka.ssl.truststore.password" -> kafkaPassword,
    "kafka.ssl.keystore.location" -> keyStoreLocation,
    "kafka.ssl.keystore.password" -> kafkaPassword,
    "kafka.ssl.key.password" -> kafkaPassword,
    "kafka.security.protocol" -> "SASL_SSL",
    "kafka.sasl.mechanism" -> "GSSAPI",
    "kafka.sasl.kerberos.service.name" -> "kafka")

  def getDF(sourceConfig: SourceConfig, startTime: BigInt, endTime: BigInt): DataFrame = {
    val metadataFields = sourceConfig.sourceFields
    val topicName = createTopicName(metadataFields.producer, metadataFields.typePrefix, metadataFields.topicType)
    val kafkaOffsets = createOffsets(startTime, endTime, topicName)
    var df = readTopic(topicName, kafkaOffsets)

    if(metadataFields.producer == "collectd") {
      // Flatten the schema for collectd data to align it with HDFS schema.
      df = df.select(col("data.*"), col("metadata.*"))
    }

    df = TimestampFilter.dataFrameInInterval(
      df = df,
      producer = metadataFields.producer,
      startTime = startTime,
      endTime = endTime
    )
    InputConfigProcess.transformDF(df, sourceConfig.transformConfig)
  }

  private def createTopicName(producer: String, typePrefix: String, topicType: String) = s"${producer}_${typePrefix}_$topicType"

  private def createOffsets(start: BigInt, end: BigInt, topicName: String): Map[String, String] = {
    val fiveMinutesMargin = 5 * 60 * 1000
    val detectionStartMillis = start * 1000
    val detectionEndMillis = end * 1000 + fiveMinutesMargin

    // create data frame from kafka topic with offset for each kafka input broker
    val partitions = kafkaInputBrokers.split(",").length
    val kafkaStartingOffsets = (0 until partitions).map(p => (p.toString, detectionStartMillis)).toMap
    val kafkaEndingOffsets = (0 until partitions).map(p => (p.toString, detectionEndMillis)).toMap

    Map(
      "startingOffsetsByTimestamp" -> Serialization.write(Map(topicName -> kafkaStartingOffsets)),
      "endingOffsetsByTimestamp" -> Serialization.write(Map(topicName -> kafkaEndingOffsets))
    )
  }

  private def readTopic(topic: String, offset: Map[String, Any] = Map()): DataFrame = {
    var kafkaInput = spark.read.format("kafka")
      .option("kafka.bootstrap.servers", kafkaInputBrokers)
      .option("subscribe", topic)

    kafkaConf ++ offset foreach {
      case (key, value) => kafkaInput = kafkaInput.option(key, value.toString)
    }

    val df = kafkaInput.load().select(col(DFColumn.JSONdata.toString).cast("string")).withDynamicSchema()
    LOGGER.info(f"Topic $topic read successfully with schema ${df.schema}")
    df
  }

  def getFromFile(spark: SparkSession, schemaPath: String): StructType = {
    spark.read.json(schemaPath).schema
  }

}
