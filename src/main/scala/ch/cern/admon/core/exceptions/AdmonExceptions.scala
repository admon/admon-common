package ch.cern.admon.core.exceptions

final case class ADMONServiceRequestException(private val message: String = "",
                                              private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

final case class ADMONServiceNoResultException(private val message: String = "",
                                              private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

final case class DetectionProjectException(private val message: String = "",
                                          private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

final case class DetectionEntityException(private val message: String = "",
                                          private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

final case class InferenceException(private val message: String = "",
                                    private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

final case class MonitException(private val message: String = "",
                                private val cause: Throwable = None.orNull)
  extends Exception(message, cause)

final case class MissingConfig(message: String = "", cause: Throwable = None.orNull) extends Exception(message, cause)


object ExceptionUtil {
  def getStackString(e: Exception): String = {
    e.getStackTrace.mkString("\n", "\n    ", "")
  }

  @annotation.tailrec
  def retry[T](n: Int)(fn: => T): T = {
    try {
      return fn
    } catch {
      case _ if n > 1 => // retry
    }
    retry(n - 1)(fn)
  }
}
