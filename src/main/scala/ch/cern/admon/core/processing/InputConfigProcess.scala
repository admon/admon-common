package ch.cern.admon.core.processing

import ch.cern.admon.core.model
import ch.cern.admon.core.model.{AggregateContent, DFColumn, InputDataConfig, TransformConfig}
import org.apache.spark.sql.expressions.Aggregator
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, Encoder, Encoders, RelationalGroupedDataset, Row}
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization

import scala.collection.mutable

object InputConfigProcess extends Serializable {
  /**
   * Processes Dataframe that comes directly from source into a preprocessed instance json.
   * It is expecting a spark dataframe from kafka or hdfs.
   * The rows will get joined, transformed, grouped and then combined by group and time interval.
   * The resulting dataframe contains a column with the aggregated rows as json string in column `instance`.
   *
   * @param sourceDFs       spark DataFrame directly from Kafka or HDFS.
   * @param inputDataConfig Configuration on how to process the source DataFrame.
   * @return spark DataFrame with `instances` column.
   */
  def processInputConfig(sourceDFs: List[DataFrame], inputDataConfig: InputDataConfig, detectionStart: Long): DataFrame = {
    val groupByColumns = inputDataConfig.groupByFields.getOrElse(List())

    inputDataConfig.joinConfig match {
      case Some(jc) => {
        val aggIntervalMS = jc.aggIntervalSeconds * 1000
        val detStartMillis = detectionStart * 1000
        val timeWindow = udf((ts: Long) => ((ts - detStartMillis) / aggIntervalMS).toLong * aggIntervalMS + detStartMillis)
        val groupByColumnsWindow = (groupByColumns :+ DFColumn.Time.toString).distinct

        val workDFs = (sourceDFs zip inputDataConfig.sourceConfigs).map(zipped_df_sc => {
          val (df, sc) = zipped_df_sc

          var fieldsOfInterest = sc.transformConfig.selectFields.get.toSet
          fieldsOfInterest = fieldsOfInterest - DFColumn.Time.toString
          for ((oldCol, newCol) <- sc.transformConfig.renameMap.getOrElse(Map())) {
            fieldsOfInterest = fieldsOfInterest - oldCol
            fieldsOfInterest = fieldsOfInterest + newCol
          }
          for (groupByCol <- groupByColumnsWindow) {
            fieldsOfInterest = fieldsOfInterest - groupByCol
          }
          val fieldsOfInterestAgg = fieldsOfInterest.toList.map(field => {
            val aggField = jc.aggMethod match {
              case "avg"  => avg(field)
              case "count"  => count(field)
              case "max"  => max(field)
              case "mean"  => mean(field)
              case "min"  => min(field)
              case "skewness"  => skewness(field)
              case "stddev"  => stddev(field)
              case "sum"  => sum(field)
              case "variance"  => variance(field)
            }
            aggField.as(field)
          })

          df.withColumn(DFColumn.Time.toString, timeWindow(col(DFColumn.Time.toString)))
            .groupBy(groupByColumnsWindow.head, groupByColumnsWindow.tail: _*)
            .agg(fieldsOfInterestAgg.head, fieldsOfInterestAgg.tail:_*)
        })

        var workDF = workDFs.head
        for (groupedDF <- workDFs.drop(1)) {
          workDF = workDF.join(groupedDF, usingColumns = groupByColumnsWindow, joinType = "outer")
        }

        val additionalMetadata: Map[String, String] = Map(
          "agg_interval_seconds" -> jc.aggIntervalSeconds.toString,
          "agg_method" -> jc.aggMethod
        )

        aggregateInstance(workDF, groupByColumns, additionalMetadata)
      }
      case _ => {
        val workDF = sourceDFs.head
        if (workDF.isEmpty) {
          // in case DataFrame is empty, adding the instances column for completeness
          return workDF.withColumn(DFColumn.Instance.toString, lit(""))
        }

        aggregateInstance(workDF, groupByColumns)
      }
    }
  }

  /**
   * Function to call filter, select and rename on spark dataframe based on
   * config parameters. First call filter, to be able to filter on columns, which might
   * not get selected later.
   *
   * @param dataFrame Spark dataframe where functions will get called on
   * @param transform Contains optional filter, select and rename configs
   * @return Spark dataframe with filter, select and rename applied
   */
  def transformDF(dataFrame: DataFrame, transform: TransformConfig): DataFrame = {
    // filter
    var workDF = if (transform.filterExpression.isDefined) {
      dataFrame.where(transform.filterExpression.get)
    } else {
      dataFrame
    }
    // select
    workDF = if (transform.selectFields.isDefined) {
      workDF.select(transform.selectFields.get.map(col): _*)
    } else {
      workDF
    }
    // rename
    for ((oldColumnName, newColumnName) <- transform.renameMap.getOrElse(Map())) {
      workDF = workDF.withColumnRenamed(oldColumnName, newColumnName)
    }
    workDF
  }

  /**
   * Creates Instance Payload as json strings for grouped columns and interval, that can be used for sending to a model.
   *
   * @param dataFrame      Incoming spark DataFrame with time series data.
   * @param groupByColumns List of columns to group by the data additionally to the time intervals.
   * @return Aggregated DataFrame with Instance column that contains json payloads.
   */
  def aggregateInstance(dataFrame: DataFrame,
                        groupByColumns: List[String],
                        additionalMetadata: Map[String, String] = Map()): DataFrame = {
    val groupedDataFrame: RelationalGroupedDataset = if(groupByColumns.nonEmpty) {
      dataFrame.groupBy(groupByColumns.head, groupByColumns.tail: _*)
    } else {
      dataFrame.groupBy()
    }
    val valueColumns = dataFrame.columns.toList filterNot groupByColumns.contains
    val aggToJSON = new AggregateToPayload(valueColumns, groupByColumns, additionalMetadata).toColumn

    groupedDataFrame.agg(aggToJSON.alias(model.Instance.toString))
  }

  class AggregateToPayload(val valueColumns: List[String],
                           val groupByColumns: List[String],
                           val additionalMetadata: Map[String, String] = Map()
                          ) extends Aggregator[Row, AggregateContent, String] {
    override def zero: AggregateContent = AggregateContent(mutable.Map(), mutable.ListBuffer())

    override def reduce(b: AggregateContent, a: Row): AggregateContent = {
      for (column <- groupByColumns) {
        b.metadata += (column -> a.getAs[String](column))
      }
      var rowMap = Map[String, String]()
      for (column <- valueColumns) {
        rowMap += (column -> a.getAs[String](column))
      }
      b.data.append(rowMap)
      b
    }

    override def merge(b1: AggregateContent, b2: AggregateContent): AggregateContent = {
      val ret = AggregateContent(
        data = b1.data ++ b2.data,
        metadata = b1.metadata ++ b2.metadata
      )
      ret
    }

    override def finish(reduction: AggregateContent): String = {
      implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats
      Serialization.write(model.Instance(
        metadata = reduction.metadata.toMap ++ additionalMetadata,
        data = reduction.data.toList
      ))
    }

    override def bufferEncoder: Encoder[AggregateContent] = Encoders.kryo[AggregateContent]

    override def outputEncoder: Encoder[String] = Encoders.STRING
  }
}
