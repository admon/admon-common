package ch.cern.admon.core.utils

object Constants {
  val ADMON_SERVICE_URL: String = "http://admon-service.cern.ch/"

  val AUTH_SERVICE_SESSION: String = DotEnv.get("AUTH_SERVICE_SESSION")

  val HDFS_MONIT_COLLECTD_ROOT_PATH: String = "hdfs://analytix/project/monitoring/collectd/"

  val HDFS_MONIT_ARCHIVE_ROOT_PATH: String = "hdfs://analytix/project/monitoring/archive/"
}