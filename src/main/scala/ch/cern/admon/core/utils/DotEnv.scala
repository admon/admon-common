package ch.cern.admon.core.utils

import scala.collection.mutable
import scala.io.{BufferedSource, Source}

object DotEnv {
  private def loadDotEnv(sourceFile: String = "resources/.env"): mutable.Map[String, String] = {
    val envFile: BufferedSource = Source.fromFile(sourceFile)
    val envList: Seq[Array[String]] = envFile.getLines.toList
      .filter((line: String) => line.nonEmpty)
      .map((line: String) => line.split("="))
    envFile.close()
    val envMap: mutable.Map[String, String] = mutable.Map()
    for (env <- envList) {
      envMap += (env(0) -> env(1))
    }
    envMap
  }

  def get(envKey: String, sourceFile: String = "resources/.env"): String = {
    sys.env.getOrElse(envKey, {
      try {
        val envMap = DotEnv.loadDotEnv(sourceFile)
        envMap(envKey)
      } catch {
        case _: Exception => ""
      }
    })
  }
}
