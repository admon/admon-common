package ch.cern.admon.core.utils

import ch.cern.admon.core.model.DFColumn
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col

object TimestampFilter {
  val guaranteeMS: (Float => Long) = (time: Float) => {
    if (time > 1.0E12) {
      time.toLong
    } else {
      (time * 1000).toLong
    }
  }

  def dataFrameInInterval(df: DataFrame, producer: String, startTime: BigInt, endTime: BigInt): DataFrame = {
    if(producer == "collectd") {
      df.filter(
        col(DFColumn.Time.toString) < (endTime * 1000).toLong &&
          col(DFColumn.Time.toString) >= (startTime * 1000).toLong
      )
    } else {
      df.filter(
        col(DFColumn.MetadataTime.toString) < (endTime * 1000).toLong &&
          col(DFColumn.MetadataTime.toString) >= (startTime * 1000).toLong
      )
    }
  }
}
