package ch.cern.admon.core.utils

import scala.concurrent.{Await, Awaitable}
import scala.concurrent.duration.Duration

object AsyncAwait {
  def await[T](fn: => Awaitable[T]): T = {
    Await.result(fn, Duration.Inf)
  }
}
