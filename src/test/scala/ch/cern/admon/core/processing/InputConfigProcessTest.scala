package ch.cern.admon.core.processing

import ch.cern.admon.core.data.KafkaMonitDataSource
import ch.cern.admon.core.model.{DFColumn, InputDataConfig, SourceConfig, SourceFields, TransformConfig}
import org.apache.spark.sql.SparkSession
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class InputConfigProcessTest extends AnyFlatSpec with Matchers with BeforeAndAfterAll {
  val spark: SparkSession = SparkSession.builder().appName("KafkaDataTest")
    .config("spark.master", "local")
    .getOrCreate()
  val kafkaSource: KafkaMonitDataSource = new KafkaMonitDataSource(spark)
  val now: Long = System.currentTimeMillis() / 1000
  val seconds: Long = 1
  val minutes: Long = 60 * seconds


  it should "Create spark dataframe with data" in {
    val metadataSourceConfig: SourceConfig = SourceConfig(
      SourceFields("collectd", "raw", "monitoring"),
      TransformConfig(
        selectFields = Some(List("value", "plugin", "type_instance", "metadata.timestamp", "metadata.version")),
        filterExpression = Some("(topic=='xrootd_raw_gled' OR topic=='xrootd_raw_alice' OR topic=='xrootd_enr_transfer') AND type_instance=='messages_in' AND plugin=='kafka'"),
        renameMap = Some(Map("metadata.version" -> "version", "metadata.timestamp" -> "timestamp"))
      ))

    val ipc = InputDataConfig(
      sourceConfigs = List(metadataSourceConfig),
      groupByFields = Some(List("plugin", "type_instance"))
    )

    var df = kafkaSource.getDF(
      sourceConfig = metadataSourceConfig,
      startTime = now - 15*minutes,
      endTime = now - 5*minutes
    )

    df = InputConfigProcess.processInputConfig(List(df), ipc, now - 15*minutes)
    df.show(truncate = false)
  }
}
