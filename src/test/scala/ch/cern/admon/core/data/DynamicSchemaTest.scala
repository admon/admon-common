package ch.cern.admon.core.data

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types.DataTypes.{DoubleType, LongType, StringType}
import org.apache.spark.sql.types.{StructField, StructType}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ch.cern.admon.core.data.DynamicSchema._
import ch.cern.admon.core.model.DFColumn
import org.scalatest.BeforeAndAfterAll


class DynamicSchemaTest extends AnyFlatSpec with Matchers with BeforeAndAfterAll {
  val spark: SparkSession = SparkSession.builder().appName("KafkaDataTest")
    .config("spark.master", "local")
    .getOrCreate()
  import spark.implicits._
  var workDF: DataFrame = List("").toDF()

  val now: Long = System.currentTimeMillis()
  val secondMillis: Long = 1000
  val minuteMillis: Long = 60 * secondMillis

  val expectedMetadataSchema = new StructType(Array(
    StructField("_id", dataType=StringType),
    StructField("availability_zone", dataType=StringType),
    StructField("event_timestamp", dataType=LongType),
    StructField("landb_service_name", dataType=StringType),
    StructField("producer", dataType=StringType),
    StructField("submitter_environment", dataType=StringType),
    StructField("submitter_hostgroup", dataType=StringType),
    StructField("timestamp", dataType=LongType),
    StructField("timestamp_format", dataType=StringType),
    StructField("toplevel_hostgroup", dataType=StringType),
    StructField("type", dataType=StringType),
    StructField("type_prefix", dataType=StringType),
    StructField("version", dataType=StringType),
  ))

  val expectedSchema = new StructType(Array(
    StructField("dstype", dataType=StringType),
    StructField("host", dataType=StringType),
    StructField("interval", dataType=DoubleType),
    StructField("metadata", dataType=expectedMetadataSchema),
    StructField("plugin", dataType=StringType),
    StructField("plugin_instance", dataType=StringType),
    StructField("time", dataType=DoubleType),
    StructField("type", dataType=StringType),
    StructField("type_instance", dataType=StringType),
    StructField("value", dataType=DoubleType),
  ))


  it should "Build a schema dynamically" in {
    workDF = workDF.withDynamicSchema()

    (expectedSchema diff workDF.schema).length shouldEqual 0
  }


  override def beforeAll(): Unit = {
    val exampleRow = """{"metadata":{"availability_zone":"cern-geneva-a","submitter_environment":"production","type":"monitoring","toplevel_hostgroup":"monitoring","event_timestamp":1646999010000,"version":"3.0","timestamp_format":"yyyy-MM-dd","submitter_hostgroup":"monitoring/kafkaconnect","type_prefix":"raw","landb_service_name":"S513-C-VM960","producer":"collectd","_id":"281e3e62-7818-b76c-2886-a2e2bcf39d27","timestamp":1646999010813},"data":{"time":1646999010.808,"interval":60.0,"host":"monit-kafkaconnect-b92425a31a.cern.ch","plugin":"GenericJMX","plugin_instance":"connector:hdfs-sink-ai_catalog_diff","type":"count","type_instance":"sink-records-active-task:10","value":0.0,"dstype":"gauge"}}"""

    val columns = List(DFColumn.JSONdata.toString)
    val data = List(
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
      (exampleRow),
    )
    workDF = data.toDF(columns:_*)
  }
}
