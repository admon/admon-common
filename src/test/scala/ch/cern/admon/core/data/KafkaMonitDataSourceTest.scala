package ch.cern.admon.core.data

import ch.cern.admon.core.model.{DFColumn, SourceConfig, SourceFields, TransformConfig}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.types.DataTypes._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class KafkaMonitDataSourceTest extends AnyFlatSpec with Matchers {
  val spark: SparkSession = SparkSession.builder().appName("KafkaDataTest")
    .master("local")
    .getOrCreate()

  val kafkaSource: KafkaMonitDataSource = new KafkaMonitDataSource(spark)
  val now: Long = System.currentTimeMillis() / 1000
  val seconds: Long = 1
  val minutes: Long = 60 * seconds

  val collectdSourceConfig: SourceConfig = SourceConfig(
    SourceFields("collectd", "raw", "monitoring"),
    TransformConfig(selectFields = Some(List("value"))))

  val expectedMetadataSchema = new StructType(Array(
    StructField("_id", dataType=StringType),
    StructField("availability_zone", dataType=StringType),
    StructField("event_timestamp", dataType=LongType),
    StructField("landb_service_name", dataType=StringType),
    StructField("producer", dataType=StringType),
    StructField("submitter_environment", dataType=StringType),
    StructField("submitter_hostgroup", dataType=StringType),
    StructField("timestamp", dataType=LongType),
    StructField("timestamp_format", dataType=StringType),
    StructField("toplevel_hostgroup", dataType=StringType),
    StructField("type", dataType=StringType),
    StructField("type_prefix", dataType=StringType),
    StructField("version", dataType=StringType),
  ))

  val expectedDataSchema = new StructType(Array(
    StructField("dstype", dataType=StringType),
    StructField("host", dataType=StringType),
    StructField("interval", dataType=DoubleType),
    StructField("metadata", dataType=expectedMetadataSchema),
    StructField("plugin", dataType=StringType),
    StructField("plugin_instance", dataType=StringType),
    StructField("time", dataType=DoubleType),
    StructField("type", dataType=StringType),
    StructField("type_instance", dataType=StringType),
    StructField("value", dataType=DoubleType),
  ))

  it should "Create spark dataframe with data and within the time interval" in {
    val startTime = now - 35*minutes
    val endTime = now - 30*minutes
    val df = kafkaSource.getDF(
      sourceConfig = collectdSourceConfig,
      startTime = startTime,
      endTime = endTime
    ).limit(100)

    (expectedDataSchema diff df.schema).length shouldEqual 0

    val Row(minValue: Long, maxValue: Long) = df.agg(
      min(DFColumn.MetadataTime.toString),
      max(DFColumn.MetadataTime.toString)
    ).head

    minValue should be >= startTime * 1000
    maxValue should be < endTime * 1000
  }
}
