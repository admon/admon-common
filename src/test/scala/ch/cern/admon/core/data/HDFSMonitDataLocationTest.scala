package ch.cern.admon.core.data

import ch.cern.admon.core.data.HDFSMonitDataLocation
import ch.cern.admon.core.model.{SourceConfig, SourceFields, TransformConfig}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class HDFSMonitDataLocationTest extends AnyFlatSpec with Matchers {
  val now: Long = System.currentTimeMillis() / 1000
  val seconds: Long = 1
  val minutes: Long = 60 * seconds
  val hours: Long = 60 * minutes
  val days: Long = 24 * hours

  val genericSourceConfig: SourceConfig = SourceConfig(SourceFields("admon", "raw", "detection"), TransformConfig(selectFields = Some(List("test"))))
  val collectdSourceConfig: SourceConfig = SourceConfig(SourceFields("collectd", "raw", "monitoring"), TransformConfig(selectFields = Some(List("test"))))


  it should "Create HDFSMonitDataLocationService" in {
    val hdfsService: HDFSMonitDataLocation = HDFSMonitDataLocation(genericSourceConfig)

    hdfsService should not be null
  }

  it should "Create Json HDFS data root path" in {
    val hdfsService: HDFSMonitDataLocation = HDFSMonitDataLocation(genericSourceConfig)

    hdfsService.buildDataLocationRootPath._1 should be("json")
    hdfsService.buildDataLocationRootPath._2.toString should be("hdfs://analytix/project/monitoring/archive/admon/raw/detection")
  }

  it should "Create Json HDFS data paths with time partition" in {
    val hdfsService: HDFSMonitDataLocation = HDFSMonitDataLocation(genericSourceConfig)
    val dataPathsWithTimePartition = hdfsService.buildPathWithDatePartitionFolders(now - 3*days, now)
    dataPathsWithTimePartition._1 should be("json")
    dataPathsWithTimePartition._2.length should be(4)
    dataPathsWithTimePartition._2(2) should include("hdfs://analytix/project/monitoring/archive/admon/raw/detection")
  }

  it should "Create Parquet HDFS data root path" in {
    val hdfsService: HDFSMonitDataLocation = HDFSMonitDataLocation(collectdSourceConfig)

    hdfsService.buildDataLocationRootPath._1 should be("parquet")
    hdfsService.buildDataLocationRootPath._2.toString should be("hdfs://analytix/project/monitoring/collectd/monitoring")
  }

  it should "Create Parquet HDFS data paths with time partition" in {
    val hdfsService: HDFSMonitDataLocation = HDFSMonitDataLocation(collectdSourceConfig)
    val dataPathsWithTimePartition = hdfsService.buildPathWithDatePartitionFolders(now - 3*days, now)
    dataPathsWithTimePartition._1 should be("parquet")
    dataPathsWithTimePartition._2.length should be(4)
    dataPathsWithTimePartition._2(2) should include("hdfs://analytix/project/monitoring/collectd/monitoring")
  }


  it should "Create HDFS path with partitions in differnt months" in {
    val hdfsService: HDFSMonitDataLocation = HDFSMonitDataLocation(collectdSourceConfig)
    val startTimestamp = 1638316800 //2021-12-01
    val endTimestamp = 1643673600 //2022-02-01
    val dataPathsWithTimePartition = hdfsService.buildPathWithDatePartitionFolders(startTimestamp, endTimestamp)
    dataPathsWithTimePartition._1 should be("parquet")
    dataPathsWithTimePartition._2.length should be(63)
    dataPathsWithTimePartition._2(2) should include("hdfs://analytix/project/monitoring/collectd/monitoring")
  }

}
