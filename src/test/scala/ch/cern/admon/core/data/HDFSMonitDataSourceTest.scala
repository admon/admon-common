package ch.cern.admon.core.data

import ch.cern.admon.core.model.{DFColumn, SourceConfig, SourceFields, TransformConfig}
import org.apache.spark.sql.functions.{max, min}
import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class HDFSMonitDataSourceTest extends AnyFlatSpec with Matchers {
  val spark: SparkSession = SparkSession.builder().appName("HDFSDataTest")
    .config("spark.master", "local")
    .config("spark.driver.memory", "4g")
    .config("spark.executor.memory", "4g")
    .config("spark.driver.cores", "2")
    .config("spark.executor.cores", "2")
    .getOrCreate()


  val hdfsSource: HDFSMonitDataSource = new HDFSMonitDataSource(spark)
  val now: Long = System.currentTimeMillis() / 1000
  val seconds: Long = 1
  val minutes: Long = 60 * seconds
  val hours: Long = 60 * minutes
  val days: Long = 24 * hours

  it should "Create spark dataframe with data and within the time interval" in {
    val collectdSourceConfig = SourceConfig(SourceFields("collectd", "raw", "monitoring"), TransformConfig(selectFields = Some(List("value"))))
    val startTime = now - (5*days + 2*hours)
    val endTime = now - 5*days
    val df = hdfsSource.getDF(collectdSourceConfig, startTime, endTime)
    df.cache()
    df.count() should be > 0L

    val Row(minValue: Long, maxValue: Long) = df.agg(
      min(DFColumn.Time.toString),
      max(DFColumn.Time.toString)
    ).head

    minValue should be >= startTime * 1000
    maxValue should be < endTime * 1000

    df.show()
  }
}
