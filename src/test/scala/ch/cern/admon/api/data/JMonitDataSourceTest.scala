package ch.cern.admon.api.data

import ch.cern.admon.api.model.{JSourceConfig, JSourceFields, JTransformConfig}
import ch.cern.admon.core.utils.DotEnv
import org.apache.hadoop.security.UserGroupInformation
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.types.DataTypes._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class JMonitDataSourceTest extends AnyFlatSpec with Matchers {
  UserGroupInformation.loginUserFromKeytab("admonops@CERN.CH", "resources/admonops.keytab")

  val spark: SparkSession = SparkSession.builder().appName("KafkaDataTest")
    .master("local")
    .getOrCreate()

  val now: Long = System.currentTimeMillis() / 1000
  val seconds: Long = 1
  val minutes: Long = 60 * seconds
  val hours: Long = 60 * minutes
  val days: Long = 24 * hours

  val selectFields = new java.util.ArrayList[java.lang.String]()
  selectFields.add("value")

  val monitDataSource: JMonitDataSource = new JMonitDataSource(
    spark = spark,
    jSourceConfig = JSourceConfig(
      JSourceFields("collectd", "raw", "cpu"),
      JTransformConfig()
    )
  )

  val expectedMetadataSchema = new StructType(Array(
    StructField("_id", dataType = StringType),
    StructField("availability_zone", dataType = StringType),
    StructField("event_timestamp", dataType = LongType),
    StructField("landb_service_name", dataType = StringType),
    StructField("producer", dataType = StringType),
    StructField("submitter_environment", dataType = StringType),
    StructField("submitter_hostgroup", dataType = StringType),
    StructField("timestamp", dataType = LongType),
    StructField("timestamp_format", dataType = StringType),
    StructField("toplevel_hostgroup", dataType = StringType),
    StructField("type", dataType = StringType),
    StructField("type_prefix", dataType = StringType),
    StructField("version", dataType = StringType),
  ))

  val expectedDataSchema = new StructType(Array(
    StructField("dstype", dataType = StringType),
    StructField("host", dataType = StringType),
    StructField("interval", dataType = DoubleType),
    StructField("metadata", dataType = expectedMetadataSchema),
    StructField("plugin", dataType = StringType),
    StructField("plugin_instance", dataType = StringType),
    StructField("time", dataType = DoubleType),
    StructField("type", dataType = StringType),
    StructField("type_instance", dataType = StringType),
    StructField("value", dataType = DoubleType),
  ))

  it should "Create spark dataframe with data from kafka" in {
    val df = monitDataSource.readKafka(
      startTime = now - 5 * minutes,
      endTime = now,
      DotEnv.get("KAFKA_INPUT_BROKER"),
      DotEnv.get("KAFKA_PASSWORD"),
    ).limit(100)
    df.show()

    (expectedDataSchema diff df.schema).length shouldEqual 0
  }

  it should "Create spark dataframe with data from hdfs" in {
    val df = monitDataSource.readHDFS(
      startTime = now - 5*days,
      endTime = now - 5*days
    ).limit(100)
    df.cache()

    df.show()
  }
}

