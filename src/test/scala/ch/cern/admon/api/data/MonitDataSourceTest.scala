package ch.cern.admon.api.data

import ch.cern.admon.core.data.MonitDataSource
import ch.cern.admon.core.model.{InputDataConfig, JoinConfig, SourceConfig, SourceFields, TransformConfig}
import ch.cern.admon.core.utils.DotEnv
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.types.DataTypes._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class MonitDataSourceTest extends AnyFlatSpec with Matchers {
  val spark: SparkSession = SparkSession.builder().appName("KafkaDataTest")
    .master("local[*]")
    .getOrCreate()

  val now: Long = System.currentTimeMillis() / 1000
  val seconds: Long = 1
  val minutes: Long = 60 * seconds
  val hours: Long = 60 * minutes
  val days: Long = 24 * hours


  val monitDataSource: MonitDataSource = new MonitDataSource(
    spark = spark,
    inputDataConfig = InputDataConfig(
      joinConfig = Some(JoinConfig(
        aggIntervalSeconds = 300,
        aggMethod = "avg"
      )),
      sourceConfigs = List(
        SourceConfig(
          sourceFields = SourceFields(
            producer = "collectd",
            typePrefix = "raw",
            topicType = "monitoring",
          ),
          transformConfig = TransformConfig(
            filterExpression = Some("topic=='xrootd_raw_gled' AND type_instance=='messages_in' AND plugin=='kafka'"),
            renameMap = Some(Map("value" -> "raw_gled_value")),
            selectFields = Some(List("timestamp", "value"))
          )
        ), SourceConfig(
          sourceFields = SourceFields(
            producer = "collectd",
            typePrefix = "raw",
            topicType = "monitoring",
          ),
          transformConfig = TransformConfig(
            filterExpression = Some("topic=='xrootd_raw_alice' AND type_instance=='messages_in' AND plugin=='kafka'"),
            renameMap = Some(Map("value" -> "raw_alice_value")),
            selectFields = Some(List("timestamp", "value"))
          )
        ), SourceConfig(
          sourceFields = SourceFields(
            producer = "collectd",
            typePrefix = "raw",
            topicType = "monitoring",
          ),
          transformConfig = TransformConfig(
            filterExpression = Some("topic=='xrootd_enr_transfer' AND type_instance=='messages_in' AND plugin=='kafka'"),
            renameMap = Some(Map("value" -> "enr_transfer_value")),
            selectFields = Some(List("timestamp", "value"))
          )
        )
      )
    )
  )

  val expectedMetadataSchema = new StructType(Array(
    StructField("_id", dataType = StringType),
    StructField("availability_zone", dataType = StringType),
    StructField("event_timestamp", dataType = LongType),
    StructField("landb_service_name", dataType = StringType),
    StructField("producer", dataType = StringType),
    StructField("submitter_environment", dataType = StringType),
    StructField("submitter_hostgroup", dataType = StringType),
    StructField("timestamp", dataType = LongType),
    StructField("timestamp_format", dataType = StringType),
    StructField("toplevel_hostgroup", dataType = StringType),
    StructField("type", dataType = StringType),
    StructField("type_prefix", dataType = StringType),
    StructField("version", dataType = StringType),
  ))

  val expectedDataSchema = new StructType(Array(
    StructField("dstype", dataType = StringType),
    StructField("host", dataType = StringType),
    StructField("interval", dataType = DoubleType),
    StructField("metadata", dataType = expectedMetadataSchema),
    StructField("plugin", dataType = StringType),
    StructField("plugin_instance", dataType = StringType),
    StructField("time", dataType = DoubleType),
    StructField("type", dataType = StringType),
    StructField("type_instance", dataType = StringType),
    StructField("value", dataType = DoubleType),
  ))

  it should "Create spark dataframe with data from kafka" in {
    val df = monitDataSource.readKafka(
      startTime = now - 25 * minutes,
      endTime = now - 15 * minutes,
      DotEnv.get("KAFKA_INPUT_BROKERS"),
      DotEnv.get("KAFKA_PASSWORD"),
    ).limit(100)
    df.show(truncate=false)
  }

  it should "Create spark dataframe with data from hdfs" in {
    val df = monitDataSource.readHDFS(
      startTime = now - (2*days + 2*hours),
      endTime = now - 2*days
    ).limit(100)
    df.cache()

    df.show()
  }
}

